const express = require("express");
const https = require("https");
const httpProxy = require("http-proxy");
const path = require("path");
const fs = require("fs");

const app = express();
const appProxy = httpProxy.createProxyServer();

/**
 * Athena/Learn SEO_UI
 */
app.all("/learn/*", (req, res) => {
  appProxy.web(req, res, {
    target: {
      host: "localhost",
      port: 3000,
    },
  });
});

/**
 * Learn BFF
 */
app.all("/learn-bff", (req, res) => {
  appProxy.web(req, res, {
    target: {
      host: "localhost",
      port: 5000,
    },
  });
});

//https://seom-learnassets.test.cheggnet.com/workflow-ui-extension/test/
/**
 * Learn Workflow UI Extension
 */
app.all("/workflow-ui-extension/*", (req, res) => {
  appProxy.web(req, res, {
    target: {
      host: "localhost",
      port: 4000,
    },
  });
});

/**
 * Capp BFF
 */
app.all("/capp-bff/*", (req, res) => {
  appProxy.web(req, res, {
    target: {
      host: "https://trunk.live.test.cheggnet.com",
    },
  });
});

/**
 * Server
 */
const start = () => {
  return https
    .createServer(
      {
        key: fs.readFileSync(path.join(__dirname, "./server.key")),
        cert: fs.readFileSync(path.join(__dirname, "./server.cert")),
      },
      app
    )
    .listen(443);
};

/**
 * Start
 */
start();
console.log(
  "Starting proxy...443 (you can use i.e. https://local.live.test.cheggnet.com just make sure you add it to /etc/hosts).  Hit Ctrl-C to stop."
);

process.on("exit", (code) => {
  if (code === 1) {
    console.log("Restarting server...");
    start();
  }
});
