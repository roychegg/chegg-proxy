# Chegg Proxy

## Installation

1. `yarn`

## Start Development

Here is the options if you wan to run Athena and tether off of "trunk" (you don't need to run BFF)

1. Make sure your `/etc/hosts` has `local.live.test.cheggnet.com` in it like so

```
127.0.0.1  local.live.test.cheggnet.com
```

1. Make sure you are running SEO_UI on `http://localhost:3000`

1. Your SEO_UI should have the following in the `.env` file:

```
BASE_URL=https://trunk.live.test.cheggnet.com/
BASE_PATH=/learn
BFF_BASE_URL=https://trunk.live.test.cheggnet.com/learn-bff
```

1. For SEO_UI:

   - Run `export NODE_TLS_REJECT_UNAUTHORIZED=0`
   - Run `yarn dev`

1. Now run the proxy here: `yarn start`

1. Open your browser and go to `https://local.live.test.cheggnet.com/learn`

1. If you run into an 'insecure request'
   - Click on "Details" or "More"
   - Type `thisisunsafe`
